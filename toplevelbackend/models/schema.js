const mongoose = require("mongoose");

const PostSchema = mongoose.Schema({
    
    firstName : String,
    lastName : String,
    emailId : String,
    phoneNumber : String,
    collegeName : String,
    department : String,
    buildingNumber : String,
    wardenName : String,
    dateCreated : {
        type : Date,
        default : Date.now
    }
})

//this below "newUser" is what the collection name in Mongodb
module.exports = mongoose.model('newUser',PostSchema);