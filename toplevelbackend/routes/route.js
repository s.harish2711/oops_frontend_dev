const express = require("express");

const router = express.Router();

const NewUserPostsSchema = require("../models/schema");

//routes to handle

//below get request gets all the data from the DB
router.get("/", async (req, res) => {
  try {
    //below find method is in build in the mongoose
    const replyFromMongo = await NewUserPostsSchema.find();
    res.json(replyFromMongo);
  } catch (err) {
    res.json({ message: err });
  }
});

//this below post creates a new user in the database
router.post("/", (req, res) => {
  const NewUserPostsSchemaObj = new NewUserPostsSchema({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    emailId: req.body.emailId,
    phoneNumber: req.body.phoneNumber,
    collegeName: req.body.collegeName,
    department: req.body.department,
    buildingNumber: req.body.buildingNumber,
    wardenName: req.body.wardenName,
  });

  //this below code saves the NewUserPostsSchemaObj post object in the DB with the help of .save() function
  const savedPost = NewUserPostsSchemaObj.save()
    .then((data) => {
      res.status(200).json(data);
    })
    .catch((err) => {
      res.json({
        message: err,
      });
    });
});

//to get a back a bool when a phone number exists or not,,,,if phone number exists return true else false
router.get("/:paramPhoneNumber", async (req, res) => {
  try {
    const replyFromMongoForSpecificPhNumber = await NewUserPostsSchema.find({
      phoneNumber: req.params.paramPhoneNumber,
    });

    if (replyFromMongoForSpecificPhNumber.length > 0) {
      res.json({ phoneNumberAvailability: true });
    } else {
      res.json({ phoneNumberAvailability: false });
    }
  } catch (err) {
    res.json({ message: "some error from retriving using phNumber" });
  }
});

//upcoming feature -- delete a user (works)
//this below code is for deleting the user when the user's specific id is provided in the params
router.delete("/:paramDeleteUser",async (req,res)=>{
try{
const removedUser = await NewUserPostsSchema.remove({_id: req.params.paramDeleteUser});
res.json(removedUser);
}catch(err){
  res.json({ message: "some error from deleting" });
}
});

//upcoming feature -- update a user (not working)
// router.patch('/:paramUpdate',async (req,res)=>{
//   try{
// const updatedUser = NewUserPostsSchema.updateOne({_id: req.params.paramUpdate},{$set:{
  
//     firstName: req.body.firstName,
//     lastName: req.body.lastName,
//     emailId: req.body.emailId,
//     phoneNumber: req.body.phoneNumber,
//     collegeName: req.body.collegeName,
//     department: req.body.department,
//     buildingNumber: req.body.buildingNumber,
//     wardenName: req.body.wardenName,
  
// }});

// res.json(updatedUser);

//   }catch(err){
//     res.json({ message: "some error from updating" });
//   }
// });

module.exports = router;


//dummy dataset for using in postman : 
// {
//   "firstName" : "harish",
//   "lastName" : "s",
//   "emailId" : "s.harish2711@gmail.com",
//   "phoneNumber" : 9876543210,
//   "collegeName" : "SSN",
//   "department" : "EEE",
//   "buildingNumber" : "4",
//   "wardenName" : "murugan"

// }
 

//routes video ref : https://www.youtube.com/watch?v=vjf774RKrLc&t=505s
//for patch route : 45 :00