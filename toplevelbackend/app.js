const express = require("express");
const mongoose = require("mongoose"); 
const app = express();
const cors = require('cors');
//you will get undefined if you try to access the body of the incoming request
const bodyParser = require('body-parser');
const port = 4000;
//import routes folder
const postsRoute = require("./routes/route");
const { remove } = require("./models/schema");
//for hiding our UN and pwd
require('dotenv/config');



//to use bodyParser 
//you will get undefined if you try to access the body of the incoming request
app.use(bodyParser.json());

//if you try to fetch the routes from react you will get the cors error
//to resolve this error we use cors middleware.
app.use(cors());

app.use('/api',postsRoute);


//connect to db
mongoose.connect(process.env.DB_CONNECTION,
{useNewUrlParser : true}
,
()=>{
    console.log("connected to db");
})

//listerning to the port
app.listen(port, () =>
  console.log(`Hello world app listening on port ${port}!`)
);

//39 :00
//https://www.youtube.com/watch?v=vjf774RKrLc&t=505s

