import React from 'react'
import styled from "styled-components";


const Container = styled.div`
  display: flex;
  width: 100vw;
  height: 100vh;
  background-image: linear-gradient(lightblue, lightyellow);
  @media only screen and (max-width: 700px) {
    display: flex;
    flex-direction: column;
  }
`;
const LeftContainer = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content :center;
  
`;
const Logo = styled.h1`
  font-size: 110px;
  font-color: white;
  font-weight: 1000;
`;
const LogoDesc = styled.p``;


const RightContainer = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content :center;
  
  @media only screen and (min-width: 700px) {
    margin-left : 0px;
  }
`;

const Button = styled.button`
 
  width: 150px;
  height: 50px;
  border: none;
  outline: none;
  background: #2f2f2f;
  color: #fff;
  font-size: 22px;
  border-radius: 40px;
  text-align: center;
  box-shadow: 0 6px 20px -5px rgba(0, 0, 0, 0.4);
  position: relative;
  overflow: hidden;
  cursor: pointer;
  margin-bottom:20px;
  margin-top:20px;
`;
const Terms = styled.h2`
margin-bottom : 20px;
`;
const UL = styled.ul`
margin-right : 20px;
`;

const FormGroup = styled.div`
 margin-left : 20px;
  color: Black;
  display: block;
  width: 300px;


`;

 const Label = styled.label`
  margin-bottom: 0.5em;
  color: Black;
  display: block;
`;

 const Input = styled.input`
  padding: 0.5em;
  color: Black;
  background: lightgreen;
  border: none;
  border-radius: 3px;
  width: 100%;
  margin-bottom: 0.5em;
`;


export default function NewUserPage1Verification() {
    return (
        <Container>
      <LeftContainer>
        <Logo>OOPS</Logo>
        <LogoDesc>OUTPASS CARING WITH CARE</LogoDesc>

        
        <br/>
        <Terms>
          Terms and Conditions:
        </Terms>
          <UL>
            <li>
        Registration:
Registration and membership is open to all natural persons over thirteen years of age who have an interest in the creation and sharing of accessible climate science.
 Users under sixteen years of age must first obtain the consent of a parent or guardian.
 </li>
            <li>
            Termination of Membership
Members can resign their membership with OOPS at any time by communicating
 their decision by mail or email (see contact details below) or by writing a message in the
  Comments area of the Profile Update page.
 </li>
 <br/>
 <br/>
 <br/>
 
 </UL>
       <br/>
      
      </LeftContainer>
     <RightContainer>
     <FormGroup>
          <Label>Mobile OTP</Label>
          <Input />
        </FormGroup>
       
        <FormGroup>
          <Label>Email OTP</Label>
          <Input />
        </FormGroup>
        <Button>Verify Both</Button>
     </RightContainer>
    </Container>
    )
}
