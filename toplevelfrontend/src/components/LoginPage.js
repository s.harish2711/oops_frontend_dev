import styled from "styled-components";
import PhoneInput from "react-phone-number-input";
import { useState, useEffect } from "react";
import axios from "axios";

const Container = styled.div`
  display: flex;
  width: 100vw;
  height: 100vh;
  background-image: linear-gradient(lightblue, lightyellow);
  @media only screen and (max-width: 600px) {
    
      display: flex;
      flex-direction : column;
    
  }
`;
const LeftContainer = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  
  
`;
const Logo = styled.h1`
  font-size: 110px;
  font-color: white;
  font-weight : 1000;

`;
const LogoDesc = styled.p``;

const RightContainer = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
const PhoneNumber = styled.span`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-bottom: 10px;
`;

const Button = styled.button`
  width: 150px;
  height: 50px;
  border: none;
  outline: none;
  background: #2f2f2f;
  color: #fff;
  font-size: 22px;
  border-radius: 40px;
  text-align: center;
  box-shadow: 0 6px 20px -5px rgba(0, 0, 0, 0.4);
  position: relative;
  overflow: hidden;
  cursor: pointer;
`;

export default function LoginPage() {
  const [phoneNumberValue, setPhoneNumberValue] = useState();
  const [fetchedData, setFetchedData] = useState("");

  useEffect(() => {
    
    console.log(fetchedData);
  }, [fetchedData]);

  const handleOnLoginClick = (e) => {
    e.preventDefault();

    async function fetchData() {
      const { data } = await axios.get(
        `http://localhost:4000/api/${phoneNumberValue}`,
        phoneNumberValue
      );
      setFetchedData(data);
    }
    fetchData();
  };

  return (
    <Container>
      <LeftContainer>
        <Logo>OOPS</Logo>
        <LogoDesc>OUTPASS CARING WITH CARE</LogoDesc>
      </LeftContainer>
      <RightContainer>
        <PhoneNumber>
          <PhoneInput
            placeholder="Enter phone number"
            value={phoneNumberValue}
            country="US"
            onChange={setPhoneNumberValue}
          />
        </PhoneNumber>

        <Button onClick={handleOnLoginClick}>LOGIN</Button>
      </RightContainer>
    </Container>
  );
}
