
import './App.css';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import LoginPage from './components/LoginPage';
import NewUserPage from './components/NewUserPage1';
import NewUserPage1Verification from './components/NewUserPage1Verification';
import LoginPageToOTP from './components/LoginPageToOTP';


function App() {
  return (
    <Router>
     <Routes>
      <Route path="/LoginPage" element={<LoginPage/>}/>
      <Route path="/NewUserPage" element={<NewUserPage/>}/>
      <Route path="/NewUserPage1Verification" element={<NewUserPage1Verification/>}/>
      <Route path="/LoginPageToOTP" element={<LoginPageToOTP/>}/>
      
    </Routes>
    </Router>
  );
}

export default App;
